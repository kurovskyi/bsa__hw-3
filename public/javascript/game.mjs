const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

const roomsPageContainerElement = document.querySelector('#rooms-page');
const gamePageContainerElement = document.querySelector('#game-page');
const roomsContainerElement = document.querySelector('#rooms-container');
const createRoomBtnElement = document.querySelector('#add-room-btn');
const leaveRoomBtnElement = document.querySelector('#quit-room-btn');
const roomNameElement = document.querySelector('#room-name');
const usersContainerElement = document.querySelector('#users-container');
const userReadyBtnElement = document.querySelector('#ready-btn');
const roomTimerElement = document.querySelector('#timer');
const gameTimerElement = document.querySelector('#game-timer');
const textContainerElement = document.querySelector('#text-container');

const modal = new tingle.modal({
	footer: true,
	stickyFooter: false,
	closeMethods: ['overlay', 'button', 'escape'],
	closeLabel: 'Close',
	onClose: function () {
		leaveRoomBtnElement.classList.remove('display-none');
		userReadyBtnElement.classList.remove('display-none');
	},
});
modal.addFooterBtn(
	'Close',
	'tingle-btn tingle-btn--danger quit-results-btn',
	function () {
		modal.close();
	}
);

let activeRoomName = '';
let activeText = '';
let enteredText = '';
let gameInterval = null;

socket.on('USERNAME_ALREADY_USED_ERROR', () => {
	alert('Username is in use! Choose other one.');
	sessionStorage.clear();
	window.location.replace('/login');
});

socket.on('ROOMS_DATA', (rooms) => {
	let preparedRoomsHtml = '';

	rooms.forEach(({ name, usersCount }) => {
		preparedRoomsHtml += `
      <div class="room">
        <span class="room__users-count">Connected: ${usersCount}</span>
        <span class="room__name">${name}</span>
        <button class="btn room-join-btn">Join</button>
      </div>
    `;
	});

	roomsContainerElement.innerHTML = preparedRoomsHtml;
});

socket.on('ROOM_ALREADY_EXISTS_ERROR', () => {
	alert('Room already exists! Choose other name.');
});

socket.on('JOIN_ROOM_MAX_USERS_REACHED_ERROR', () => {
	alert('Room is full. Try another one.');
});

socket.on('JOINED_ROOM_TRIGGER', () => {
	roomsPageContainerElement.classList.add('display-none');
	gamePageContainerElement.classList.remove('display-none');
});

socket.on('LEAVED_ROOM_TRIGGER', () => {
	roomsPageContainerElement.classList.remove('display-none');
	gamePageContainerElement.classList.add('display-none');
});

socket.on('ROOM_DATA', ({ name, users }) => {
	activeRoomName = name;
	roomNameElement.innerText = name;

	let preparedUsersHtml = '';

	users.forEach(({ name, isReady }) => {
		preparedUsersHtml += `
      <div class="user-card">
        <div class="ready-status-${isReady ? 'green' : 'red'}"></div>
        <span class="user__name">${name} ${
			name === username ? '(you)' : ''
		}</span>
        <div class="user-progress ${name}">
          <span></span>
        </div>
      </div>
    `;
	});

	usersContainerElement.innerHTML = preparedUsersHtml;
});

socket.on('ROOM_TIMER_TRIGGER', (seconds) => {
	leaveRoomBtnElement.classList.add('display-none');
	userReadyBtnElement.classList.add('display-none');
	roomTimerElement.classList.remove('display-none');

	let leftSeconds = seconds;

	roomTimerElement.innerText = leftSeconds;

	const interval = setInterval(() => {
		if (leftSeconds === 0) {
			return clearInterval(interval);
		}
		leftSeconds -= 1;
		roomTimerElement.innerText = leftSeconds;
	}, 1000);
});

socket.on('START_GAME_TRIGGER', async ({ textIndex, timer }) => {
	const response = await fetch(`/game/texts/${textIndex}`);
	const text = await response.text();
	activeText = text;

	roomTimerElement.classList.add('display-none');
	textContainerElement.classList.remove('display-none');
	textContainerElement.innerHTML = text;
	gameTimerElement.classList.remove('display-none');
	gameTimerElement.innerText = `${timer} seconds left.`;

	let gameTimer = timer;

	gameInterval = setInterval(() => {
		if (gameTimer === 0) {
			return clearInterval(gameInterval);
		}
		//END GAME EMIT
		gameTimer -= 1;
		gameTimerElement.innerText = `${gameTimer} seconds left`;
	}, 1000);

	window.addEventListener('keypress', handleKeyPress);
});

socket.on('PROGRESS_TRIGGER', ({ socketUsername, progress }) => {
	const progressElement = document.querySelector(
		`.user-progress.${socketUsername}`
	);
	if (progress === 100) {
		progressElement.classList.add('won');
	}
	progressElement.querySelector(`span`).style.width = `${progress}%`;
});

socket.on('END_GAME_TRIGGER', (users) => {
	if (gameInterval) {
		clearInterval(gameInterval);
	}

	activeText = '';
	enteredText = '';
	textContainerElement.classList.add('display-none');
	gameTimerElement.classList.add('display-none');
	userReadyBtnElement.classList.remove('ready');
	userReadyBtnElement.innerText = "I'm ready";

	window.removeEventListener('keypress', handleKeyPress);

	modal.setContent(
		users?.length
			? users.reduce(
					(acc, next, i) =>
						(acc += `<p id="place-${i + 1}">${i + 1}. ${next}</p>`),
					''
			  )
			: 'Nobody won.'
	);
	modal.open();
});

function handleKeyPress({ key }) {
	if (key === activeText[enteredText.length]) {
		enteredText += activeText[enteredText.length];
		const activeTextCopy = activeText;
		textContainerElement.innerHTML = activeTextCopy.replace(
			enteredText,
			`<span class="mark">${enteredText}</span>`
		);

		const progress = (enteredText.length * 100) / activeText.length;

		const progressElement = document.querySelector(
			`.user-progress.${username}`
		);

		if (progress === 100) {
			progressElement.classList.add('won');
		}

		progressElement.querySelector(`span`).style.width = `${progress}%`;

		socket.emit('PROGRESS_TRIGGER', { roomName: activeRoomName, progress });
	}
}

createRoomBtnElement.addEventListener('click', () => {
	const roomName = prompt('Enter room name:');

	socket.emit('CREATE_ROOM_TRIGGER', roomName);
});

roomsContainerElement.addEventListener('click', (e) => {
	const targetElement = e.target;

	if (!targetElement.classList.contains('room-join-btn')) {
		return;
	}

	const roomName =
		targetElement.parentElement.querySelector('.room__name').innerText;

	socket.emit('JOIN_ROOM_TRIGGER', roomName);
});

leaveRoomBtnElement.addEventListener('click', () => {
	socket.emit('LEAVE_ROOM_TRIGGER', activeRoomName);
});

userReadyBtnElement.addEventListener('click', () => {
	if (userReadyBtnElement.classList.contains('ready')) {
		userReadyBtnElement.classList.remove('ready');
		userReadyBtnElement.innerText = "I'm ready";
		socket.emit('NOT_READY_TRIGGER', activeRoomName);
		return;
	}

	userReadyBtnElement.classList.add('ready');
	userReadyBtnElement.innerText = "I'm not ready";
	socket.emit('READY_TRIGGER', activeRoomName);
});
