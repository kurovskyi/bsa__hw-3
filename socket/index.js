import { handleRoomsLogic } from './rooms';
import { handleGameLogic } from './game';

export default (io) => {
	io.on('connection', (socket) => {
		// console.log('all', username, allSock);

		handleRoomsLogic(io, socket);
		handleGameLogic(io, socket);
	});
};
